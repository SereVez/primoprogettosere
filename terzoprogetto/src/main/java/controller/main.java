package controller;

//import java.sql.Connection;
//import java.sql.DriverManager;
import java.util.ArrayList;

//import dao.ConnectionManager;
import dao.DAOPersona;
import dao.DaoPersonaImpl;
import model.Persona;
import utils.Utils;

public class main {

	public static void main(String[] args) {
		
	/*	try {
			String url = "jdbc:mysql://localhost:3306/provaconnessione?useSSL=false&serverTimezone=UTC";
			String user = "root";
			String password = "root";
			Connection connection= DriverManager.getConnection(url, user, password);
			System.out.println("Stai connesso");
			
		}
		catch (Exception e) {
		e.printStackTrace();	
		}
		*/
	
	DAOPersona daoPersona= DaoPersonaImpl.getInstance();
	
	//create
	Persona persona= new Persona(1,"Emilio", "Miraglia");
	Persona persona2= new Persona(2,"Mauro", "Albano");
	Persona persona3= new Persona("Pietro", "Cassese");
	daoPersona.createPersona(persona2);
	daoPersona.createPersona(persona3);
	
	
	//read
	System.out.println("Lettura di una persona");
//	Persona lettura=daoPersona.readPersona(3);
	//System.out.println(lettura);
	Persona lettura1= daoPersona.readPersona(1);
	System.out.println(lettura1);
	
	//update
	persona.setNome("Emi");
	daoPersona.updatePersona(persona);
	
	//delete
	daoPersona.deletePersona(persona2);
	
	//readall
	System.out.println("Read all");
	
	Utils.stampaLista(daoPersona.readAll());
	
	//alternativa al metodo generico
	
	ArrayList<Persona> p = (ArrayList<Persona>) daoPersona.readAll();
	
	for (Persona persona4 : p) {
		System.out.println(persona4);
	}
	
	System.out.println("Fine operazione");	
	
	
	
	}

}