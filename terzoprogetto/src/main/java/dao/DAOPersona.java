package dao;

import java.util.List;

import model.Persona;

public interface DAOPersona {

	void createPersona(Persona persona);
	 
	Persona readPersona(int id);
	
	void updatePersona(Persona persona);
	
	void deletePersona(Persona persona);
	
	List<Persona> readAll();
	
	
	
	
}
