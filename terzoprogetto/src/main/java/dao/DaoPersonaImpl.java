package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Persona;

public class DaoPersonaImpl implements DAOPersona {
	
	//esempio corretto di uso del singleton 
	
	private static DaoPersonaImpl instance= null;
	
	private DaoPersonaImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public static DaoPersonaImpl getInstance() {
		if(instance==null) {
			instance=new DaoPersonaImpl();
		}
		return instance;
	}
	
	
	
	
	
	
	

	@Override
	public void createPersona(Persona persona) {

		String sql = "INSERT INTO persona (id, nome, cognome) VALUES (?, ?, ?)";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setInt(1, persona.getId());
			stm.setString(2, persona.getNome());
			stm.setString(3, persona.getCognome());
			

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}



	}

	@Override
	public Persona readPersona(int id) {

		String sql = "SELECT * FROM persona WHERE id=?";
		Persona persona= null;

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			persona = new Persona();
			stm.setInt(1, id);
			ResultSet result = stm.executeQuery();

			while (result.next()) {

				persona.setId(result.getInt("id"));
				persona.setNome(result.getString("nome"));
				persona.setCognome(result.getString("cognome"));
			

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return persona;
	}

	@Override
	public void updatePersona(Persona persona) {

		String sql = "UPDATE persona SET nome=?, cognome=? WHERE id=?";

		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);) {

			stm.setString(1, persona.getNome());
			stm.setString(2, persona.getCognome());
			stm.setInt(3, persona.getId());

			stm.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void deletePersona(Persona persona) {
	
		String sql= "DELETE FROM persona where id=?";
		try (PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);){
			
			stm.setInt(1, persona.getId());
			stm.execute();
			
			}catch (SQLException e) {
				e.printStackTrace();
			}
		
		
		
		
	}

	@Override
	public List<Persona> readAll() {

		String sql = "SELECT * FROM persona";
		List<Persona> listaPersone= null;
		try(PreparedStatement stm = ConnectionManager.getConnection().prepareStatement(sql);
				ResultSet result= stm.executeQuery(sql)) {
			
			listaPersone= new ArrayList<>();
			while(result.next()) {
				Persona persona = new Persona();
				
				persona.setId(result.getInt("id"));
				persona.setNome(result.getString("nome"));
				persona.setCognome(result.getString("cognome"));
				
				listaPersone.add(persona);				
				
			}
			
			
		} catch (SQLException e) {
		e.printStackTrace();
		}
		
		
	
		
		
		
		
		return listaPersone;
	}

}
